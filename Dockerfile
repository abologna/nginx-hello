FROM nginx
EXPOSE 8080
COPY nginx.conf /etc/nginx/nginx.conf
COPY html /usr/share/nginx/html
